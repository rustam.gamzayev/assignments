import unittest
from driver.role_controller import select, insert, update
from driver.member_controller import select as member_select


class RoleTestCase(unittest.TestCase):
    def test_select(self):
        self.assertTrue(len(select()) != 0)

    def test_insert(self):
        rows = member_select()
        row = rows[len(rows) - 1]
        role = (row[0], 'insert_test')
        insert(role[0], role[1])
        rows = select()
        row = rows[len(rows) - 1]
        self.assertEqual(row[1], role[0])
        self.assertEqual(row[2], role[1])

    def test_update(self):
        member_rows = member_select()
        role_rows = select()
        role_row = role_rows[len(role_rows) - 1]
        role = (role_row[0], member_rows[0][0], 'update_test')
        update(role[0], role[1], role[2])
        role_rows = select()
        role_row = role_rows[len(role_rows) - 1]
        self.assertEqual(role_row, role)


if __name__ == '__main__':
    unittest.main(verbosity=2)
