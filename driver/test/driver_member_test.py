import unittest
from driver.member_controller import select, insert, update


class MemberTestCase(unittest.TestCase):
    def test_select(self):
        self.assertTrue(len(select()) != 0)

    def test_insert(self):
        member = ('insert_test', 'insert_test', '1999-02-09', 'actor')
        insert(member[0], member[1], member[2], member[3])
        rows = select()
        last_row = rows[len(rows) - 1]
        self.assertEqual(last_row[1], member[0])
        self.assertEqual(last_row[2], member[1])
        self.assertEqual(last_row[3].strftime('%Y-%m-%d'), member[2])
        self.assertEqual(last_row[4], member[3])

    def test_update(self):
        rows = select()
        row = rows[len(rows) - 1]
        member = (row[0], 'update_test', 'update_test', '1999-02-09', 'actor')
        update(member[0], member[1], member[2], member[3], member[4])
        rows = select()
        row = rows[len(rows) - 1]
        self.assertEqual(row[0], member[0])
        self.assertEqual(row[1], member[1])
        self.assertEqual(row[2], member[2])
        self.assertEqual(row[3].strftime('%Y-%m-%d'), member[3])
        self.assertEqual(row[4], member[4])


if __name__ == '__main__':
    unittest.main(verbosity=2)
