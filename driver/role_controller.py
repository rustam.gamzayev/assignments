from driver.connection import Connection


def select():
    connection = Connection.get_connection()
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM role')
    rows = cursor.fetchall()
    cursor.close()
    connection.close()
    return rows


def insert(*args):
    sql = """INSERT INTO role VALUES(%s, %s, %s)"""
    id = Connection.get_new_id('role')
    connection = Connection.get_connection()
    cursor = connection.cursor()
    cursor.execute(sql, (id, args[0], args[1]))
    connection.commit()
    cursor.close()
    connection.close()


def update(*args):
    sql = """UPDATE role 
                SET member_id = %s, title = %s
                WHERE id = %s"""
    connection = Connection.get_connection()
    cursor = connection.cursor()
    cursor.execute(sql, (args[1], args[2], args[0]))
    connection.commit()
    cursor.close()
    connection.close()
