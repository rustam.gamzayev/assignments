from driver.connection import Connection


def select():
    connection = Connection.get_connection()
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM member')
    rows = cursor.fetchall()
    cursor.close()
    connection.close()
    return rows


def insert(*args):
    sql = """INSERT INTO member VALUES(%s, %s, %s, %s, %s)"""
    id = Connection.get_new_id('member')
    connection = Connection.get_connection()
    cursor = connection.cursor()
    cursor.execute(sql, (id, args[0], args[1], args[2], args[3]))
    connection.commit()
    cursor.close()
    connection.close()


def update(*args):
    sql = """UPDATE member 
                SET name = %s, surname = %s, birthday = %s, status = %s
                WHERE id = %s"""
    connection = Connection.get_connection()
    cursor = connection.cursor()
    cursor.execute(sql, (args[1], args[2], args[3], args[4], args[0]))
    connection.commit()
    cursor.close()
    connection.close()
