import psycopg2
from config import config


class Connection:
    __connection = None

    @staticmethod
    def get_connection():
        try:
            params = config()
            Connection.__connection = psycopg2.connect(**params)
            return Connection.__connection
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

    @staticmethod
    def get_new_id(table_name):
        sql = "SELECT * FROM " + table_name
        connection = Connection.get_connection()
        cursor = connection.cursor()
        cursor.execute(sql)
        rows = cursor.fetchall()
        cursor.close()
        connection.close()
        if len(rows) == 0:
            return 1
        else:
            return rows[len(rows) - 1][0] + 1
