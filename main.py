import unittest
from driver.test.driver_member_test import MemberTestCase
from driver.test.driver_role_test import RoleTestCase
from orm.test.orm_member_test import MemberModelTestCase
from orm.test.orm_role_test import RoleModelCaseTest


if __name__ == '__main__':
    unittest.main(verbosity=2)