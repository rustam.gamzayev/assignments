from orm.connection import Connection
from orm.model import Role


def select():
    session = Connection.get_session()
    rows = session.query(Role).all()
    session.close()
    return rows


def insert(role):
    session = Connection.get_session()
    session.add(role)
    session.commit()
    session.close()


def update(role):
    session = Connection.get_session()
    old_role = session.query(Role).filter(Role.id == role.id).one()
    old_role.member_id = role.member_id
    old_role.title = role.title
    session.commit()
    session.close()
