from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from config import config


class Connection:
    __session = None

    @staticmethod
    def get_session():
        cfg = config()
        engine = create_engine('postgresql://' + cfg['user'] + ":" + cfg['password'] + "@" + cfg['host']
                               + ':' + cfg['port'] + '/' + cfg['database'])
        Session = sessionmaker(bind=engine, expire_on_commit=False)
        Connection.__session = Session()
        return Connection.__session
