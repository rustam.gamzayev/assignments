import unittest
from orm.model import Member, Role
from orm.role_controller import select, insert, update
from orm.member_controller import select as member_select


class RoleModelCaseTest(unittest.TestCase):
    def test_select(self):
        self.assertTrue(len(select()) != 0)

    def test_insert(self):
        member_rows = member_select()
        role = Role(Role.get_new_id(), member_rows[0].id, 'insert_test')
        insert(role)
        rows = select()
        row = rows[len(rows) - 1]
        self.assertEqual(row.id, role.id)
        self.assertEqual(row.member_id, role.member_id)
        self.assertEqual(row.title, role.title)

    def test_update(self):
        member_rows = member_select()
        rows = select()
        row = rows[len(rows) - 1]
        role = Role(row.id, member_rows[0].id, 'update_test')
        update(role)
        rows = select()
        row = rows[len(rows) - 1]
        self.assertEqual(row.id, role.id)
        self.assertEqual(row.member_id, role.member_id)
        self.assertEqual(row.title, role.title)


if __name__ == '__main__':
    unittest.main(verbosity=2)
