import unittest
from orm.model import Member
from orm.member_controller import select, insert, update


class MemberModelTestCase(unittest.TestCase):
    def test_select(self):
        self.assertTrue(len(select()) != 0)

    def test_insert(self):
        member = Member(Member.get_new_id(), 'insert_test', 'insert_test', '1999-02-09', 'actor')
        insert(member)
        rows = select()
        row = rows[len(rows) - 1]
        self.assertEqual(row.id, member.id)
        self.assertEqual(row.name, member.name)
        self.assertEqual(row.surname, member.surname)
        self.assertEqual(row.birthday.strftime('%Y-%m-%d'), member.birthday)
        self.assertEqual(row.status, member.status)

    def test_update(self):
        rows = select()
        row = rows[len(rows) - 1]
        member = Member(row.id, 'update_test', 'update_test', '1999-02-09', 'actor')
        update(member)
        rows = select()
        row = rows[len(rows) - 1]
        self.assertEqual(row.id, member.id)
        self.assertEqual(row.name, member.name)
        self.assertEqual(row.surname, member.surname)
        self.assertEqual(row.birthday.strftime('%Y-%m-%d'), member.birthday)
        self.assertEqual(row.status, member.status)


if __name__ == '__main__':
    unittest.main(verbosity=2)
