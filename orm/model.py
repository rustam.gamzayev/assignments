from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Date, ForeignKey
from sqlalchemy.orm import relationship

from orm.connection import Connection

Base = declarative_base()


class Member(Base):
    __tablename__ = 'member'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    surname = Column(String)
    birthday = Column(Date)
    status = Column(String)
    roles = relationship('Role')

    def __init__(self, id, name, surname, birthday, status):
        self.id = id
        self.name = name
        self.surname = surname
        self.birthday = birthday
        self.status = status

    @staticmethod
    def get_new_id():
        session = Connection.get_session()
        rows = session.query(Member).all()
        session.close()
        if len(rows) == 0:
            return 1
        else:
            return rows[len(rows) - 1].id + 1


class Role(Base):
    __tablename__ = 'role'

    id = Column(Integer, primary_key=True, autoincrement=True)
    member_id = Column(Integer, ForeignKey('member.id'), nullable=False)
    title = Column(String)

    def __init__(self, id, member_id, title):
        self.id = id
        self.member_id = member_id
        self.title = title

    @staticmethod
    def get_new_id():
        session = Connection.get_session()
        rows = session.query(Role).all()
        session.close()
        if len(rows) == 0:
            return 1
        else:
            return rows[len(rows) - 1].id + 1
