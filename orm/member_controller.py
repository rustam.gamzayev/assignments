from orm.connection import Connection
from orm.model import Member


def select():
    session = Connection.get_session()
    rows = session.query(Member).all()
    session.close()
    return rows


def insert(member):
    session = Connection.get_session()
    session.add(member)
    session.commit()
    session.close()


def update(member):
    session = Connection.get_session()
    old_member = session.query(Member).filter(Member.id == member.id).one()
    old_member.name = member.name
    old_member.surname = member.surname
    old_member.birthday = member.birthday
    old_member.status = member.status
    session.commit()
    session.close()
